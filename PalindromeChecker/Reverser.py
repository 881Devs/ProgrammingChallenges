word = input("Please enter a word to reverse: ")

for char in reversed( word ):  
  print( char, end = "" )

if word == ''.join(reversed(word)):
    print("\n We have a Palidrome!")

else:
    print("\n This word is not a palindrome.")
