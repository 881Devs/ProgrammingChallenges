import random
options = ["rock","paper","scissors"]
score=0

games=int(input("How many games do you want to play?: "))

for i in range((games)):
    selection=input("Enter selection: ").lower()
    print("")
    comp=random.choice(options)

    if selection=="rock" and comp=="rock":
        print("Computer has gone for:",comp)
        print("Nobody wins")
        print("")
    elif selection=="rock" and comp=="scissors":
        print("Computer has gone for:",comp)
        print("You win!")
        score=score+1
        print("")
    elif selection=="rock" and comp=="paper":
        print("Computer has gone for:",comp)
        print("Computer wins!")
        print("")
        score=score-1
    elif selection=="paper" and comp=="rock":
        print("Computer has gone for:",comp)
        print("You win!")
        print("")
        score=score+1
    elif selection=="paper" and comp=="paper":
        print("Computer has gone for:",comp)
        print("Nobody wins!")
        print("")
    elif selection=="paper" and comp=="scissors":
        print("Computer has gone for:",comp)
        print("Computer wins!")
        print("")
        score=score-1
    elif selection=="scissors" and comp=="rock":
        print("Computer has gone for:",comp)
        print("Computer wins!")
        print("")
        score=score-1
    elif selection=="scissors" and comp=="scissors":
        print("Computer has gone for:",comp)
        print("Nobody wins!")
        print("")
    elif selection=="scissors" and comp=="paper":
        print("Computer has gone for:",comp)
        print("You win!")
        print("")
        score=score+1

print("Your final score is:",score)
